const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const fs = require('fs');

const Config = require('./Config');

app.get('/', function (req, res) {
    res.send('<h1>Hello world</h1>');
});

const server = http.listen(3002, function () {
    console.log('listening on *:3000');
});

const connectedUsers = {};
const NAME_NOT_PROVIDED = 'User name not provided';
const NAME_ALREADY_TAKEN = 'User name already taken';
const NAME_TOO_LONG = 'User name too long';


function disconnect(socket, reason, broadcast) {
    return new Promise((res, rej) => {
        if (connectedUsers[socket.handshake.query.name]) {
            console.log(reason);

            if (broadcast) {
                io.emit('chat message', JSON.stringify({
                    who: socket.handshake.query.name,
                    text: reason,
                    when: new Date().getTime(),
                }));
            }

            socket.emit('closeReason', reason);
            setTimeout(() => {
                socket.disconnect(true);
                res();
            }, 100);
            if (connectedUsers[socket.handshake.query.name]) {
                delete connectedUsers[socket.handshake.query.name];
            }
        } else {
            res();
        }

    });
}

io.use(function (socket, next) {


    if (!socket.handshake.query.name) {
        connectedUsers[socket.handshake.query.name] = {
            socket
        };
        disconnect(socket, NAME_NOT_PROVIDED)
    } else if (socket.handshake.query.name.length > Config.maxNickLength) {
        connectedUsers[socket.handshake.query.name] = {
            socket
        };
        disconnect(socket, NAME_TOO_LONG)
    } else if (connectedUsers[socket.handshake.query.name]) {
        connectedUsers[socket.handshake.query.name] = {
            socket
        };
        disconnect(socket, NAME_ALREADY_TAKEN)
    } else {
        connectedUsers[socket.handshake.query.name] = {
            socket
        };
        refreshUserInactivity(socket.handshake.query.name);
    }
    next();
});

function refreshUserInactivity(name) {
    clearTimeout(connectedUsers[name].timeout);
    connectedUsers[name].timeout = setTimeout(() => {
        if (connectedUsers[name]) {
            disconnect(connectedUsers[name].socket, 'Disconnected due to innactivity', true);
        }
    }, Config.inactivityTimeout);
}

io.on('connection', function (socket) {
    console.log('a user connected', socket.handshake.query.name.substring(0, Config.maxNickLength));
    socket.on('disconnect', function (reason) {
        console.log('user disconnected');
        if (reason === 'client namespace disconnect') {
            reason = 'Left the chat, disconnected';
        }
        disconnect(socket, reason, true);
    });

    socket.on('chat message', function (msg) {
        msg = msg.substring(0, Config.maxMessageLength);
        console.log('message: ' + msg);
        io.emit('chat message', JSON.stringify({
            who: socket.handshake.query.name,
            text: msg,
            when: new Date().getTime(),
        }));

        refreshUserInactivity(socket.handshake.query.name);
    });
});

const logStream = fs.createWriteStream('./chatServer.log');

console.log('Redirecting console to chatServer.log');

process.stderr.write = process.stdout.write = (chunk, encoding, callback) => {
    logStream.write(new Date().toLocaleString() + " : " + chunk, encoding, callback);
};

function gracefulShutdown() {
    const usersDisconnected = Promise.all(Object.entries(connectedUsers).map(([name, userObject]) => {
        return disconnect(userObject.socket, 'server shutdown');
    })).then(() => {
        console.log('Users disconnected');
    });


    const socketServerClosed = new Promise((res, rej) => io.close(res))
        .then(() => console.log('Socket server closed.'));


    const httpServerClosed = new Promise((res) => {
        server.close(res);
    }).then(() => {
        console.log('Http server closed.');
    });

    Promise.all([usersDisconnected, httpServerClosed, socketServerClosed]).then(() => {
        console.log('shutting down');
        logStream.end(() => {
            process.exit(0);
        });
    });
}

process.on('SIGINT', () => {
    console.info('SIGINT signal received.');

    gracefulShutdown();
});
process.on('SIGTERM', () => {
    console.info('SIGTERM signal received.');

    gracefulShutdown();
});



//DONE 1. Sends received messages to all connected clients (no rooms).
//DONE 2. If a client is silent for more than a certain (configurable) amount of time, it is disconnected; a message about the event (e.g. "John was disconnected due to inactivity") is sent to all connected clients.
//DONE 3. If a client is disconnected, but not due to inactivity, a different message is sent (e.g."John left the chat, connection lost" instead.)
//DONE 4. Doesn't allow multiple active users with the same nickname.
//DONE 5. Validates data received over the network, just limited it by length
//DONE 6. Terminates gracefully upon receiving SIGINT or SIGTERM signals.
//DONE 7. Provide readable logging solution, readable up to debate though, plus this solution does not log to console

//TODO way I send disconnection reason is weird, if I send and disconnect immediatly it does not reach