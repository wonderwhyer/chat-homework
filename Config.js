module.exports = {
    inactivityTimeout: 15000,
    maxNickLength: 15,
    maxMessageLength: 200
};