# Chat prototype application using React/TypeScript/Express/Socket.io/React-md

Tested on
windows 10
Node 8.11.1
Yarn 1.13.0
Chrome

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm/yarn start-all`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

As well runs server part
You can see server logs in access.log

### `npm/yarn test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

Puppeteer is used for e2e tests and some basic spec part has tests but probably less then half


## Postmortem

For validation was not sure. Considering we are not using SQl database and we are using React without dangerous HTML things
SQL injection and XSS attacks should not be possible
Only thing that comes to mind is that we are writing logs to disk
But I limited size of nicknames and messages so I guess that counts for validation
Still needs more thoughts put in to that

### Things that went well

- Socket.io was pretty straight forward
- End app started to feel good

### Things that went bad

- React-MD is not that easy to use
- Resulting UI is not responsive
- Needs refactoring of how login animation works and splitting of UI in to parts
- Chat is not scrollable and honestly not that well styled
- E2E tests for such thing are bit hard to pull off, just needs some practice