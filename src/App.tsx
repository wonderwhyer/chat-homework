import React, {Component} from 'react';
import './App.css';
import {CircularProgress, TextField, Grid, Cell, List, Subheader, ListItem, Button, Avatar, FontIcon} from 'react-md';
import {ChatState, IChatMessage} from "./ChatState";
import {ChatList} from "./ChatList";

const StarIcon = () => <FontIcon>star</FontIcon>;

const speed = '0.8s'
const transition = `width ${speed}, min-height ${speed}, height ${speed}, margin ${speed}, opacity ${speed}`;

interface IAppState {
    page: string;
    name: string;
    message: string;
    chat: IChatMessage[];
    error: string;
}

class App extends Component {
    chatState: ChatState;
    state: IAppState;

    constructor(props: any) {
        super(props);

        this.chatState = new ChatState((chatState: ChatState) => {
            this.setState(App.chatStateToUIState(chatState));
        });

        this.state = {name: '', message: '', ...App.chatStateToUIState(this.chatState)};
    }

    static chatStateToUIState(chatState: ChatState) {
        return {
            page: chatState.socket ? 'chat' : 'login',
            chat: chatState.chat.concat(),
            error: chatState.error,
        };
    }

    nameChange = (value: string | number) => {
        this.setState({
            name: value.toString(),
        })
    };

    render() {

        //return (<CircularProgress id={''} scale={2}/>);
        return (
            <Grid style={{
                height: '100%'
            }}>
                <Cell size={this.state.page === 'login' ? 2 : 12}
                      offset={this.state.page === 'login' ? 5 : 0}
                      style={{
                          transition,
                      }}
                >
                    <div className="md-paper md-paper--1"
                         style={{
                             minHeight: this.state.page === 'login' ? '0%' : '100%',
                             transition,
                             position: 'relative',
                             paddingBottom: '60px',
                         }}
                    >
                        <h2 style={{
                            padding: '8px',
                        }}>Chat</h2>

                        <Cell size={10}
                              offset={1}
                              style={{
                                  transition,
                                  margin: this.state.page === 'chat' ? '0%' : undefined,
                                  opacity: this.state.page === 'chat' ? 0 : 1,
                                  maxHeight: this.state.page === 'chat' ? '0%' : '100%',
                              }}
                        >
                            <TextField
                                id="nickname"
                                lineDirection="center"
                                placeholder="Choose a nickname"
                                disabled={this.state.page === 'chat'}
                                value={this.state.name}
                                onChange={this.nameChange}
                                error={!!this.state.error}
                                errorText={this.state.error}
                            />
                        </Cell>

                        <Cell size={12} className='md-paper md-paper--1' style={{
                            margin: this.state.page === 'chat' ? undefined : '0%',
                            opacity: this.state.page === 'chat' ? 1 : 0,
                            overflow: 'hidden',
                            transition,
                            position: 'absolute',
                            top: '90px',
                            bottom: '90px',
                            backgroundColor: '#fff',
                        }}>
                            <ChatList messages={this.state.chat}/>
                        </Cell>

                        <Cell size={12} style={{
                            bottom: 0,
                            position: 'absolute',
                        }}>
                            <Grid>
                                <Cell
                                    size={this.state.page === 'chat' ? 8 : 0}
                                    style={{
                                        transition,
                                        opacity: this.state.page === 'chat' ? 1 : 0,
                                        width: this.state.page === 'chat' ? undefined : '0%',
                                        margin: this.state.page === 'chat' ? undefined : '0%',
                                    }}
                                >
                                    <div ref={this.setMessageRef}>
                                        <TextField
                                            id="message"
                                            lineDirection="center"
                                            placeholder="Type your message and hit enter"
                                            value={this.state.message}
                                            onChange={this.onMessageChange}
                                        />
                                    </div>
                                </Cell>

                                <Cell
                                    size={this.state.page === 'chat' ? 2 : 0}
                                    style={{
                                        opacity: this.state.page === 'chat' ? 1 : 0,
                                        width: this.state.page === 'chat' ? undefined : '0%',
                                        margin: this.state.page === 'chat' ? undefined : '0%',
                                        transition,
                                    }}
                                >
                                    <Button
                                        id={'send'}
                                        raised
                                        primary
                                        style={{
                                            width: '100%',
                                            textAlign: 'center',
                                        }}
                                        onClick={this.sendMessage}
                                    >Send</Button>
                                </Cell>

                                <Cell
                                    size={this.state.page === 'chat' ? 2 : 12}
                                    style={{
                                        transition,
                                    }}
                                >
                                    <Button
                                        id={'loginLogOut'}
                                        raised
                                        primary={this.state.page == 'login'}
                                        secondary={this.state.page == 'chat'}
                                        style={{
                                            width: '100%',
                                            textAlign: 'center',
                                        }}
                                        onClick={this.loginLogOut}
                                    >{this.state.page == 'login' ? 'join' : 'logout'}</Button>
                                </Cell>
                            </Grid>
                        </Cell>
                    </div>
                </Cell>
            </Grid>
        );
    }

    onMessageChange = (value: string | number) => {
        this.setState({
            message: value.toString(),
        })
    };

    sendMessage = () => {
        this.chatState.sendMessage(this.state.message);
        this.setState({
            message: '',
        });
    };

    loginLogOut = () => {
        if (this.chatState.socket) {
            this.chatState.disconnect();
        } else {
            this.chatState.connect(this.state.name);
        }
    }

    textParent: HTMLDivElement | null | undefined;

    setMessageRef = (ref:HTMLDivElement | null) => {
        if (ref != this.textParent) {
            if (this.textParent) {

            }
            this.textParent = ref;
            //const input = this.textParent && this.textParent.querySelector('input');

            this.textParent && this.textParent.addEventListener('keydown', (e:Event) => {
                if(e instanceof KeyboardEvent) {
                    if (e.code === 'Enter') {
                        this.sendMessage();
                    }
                }
            }, {capture: true});
        }
    }
}

export default App;

//DONE 1. Has two pages landing page (shown when not connected to the server) and chat (shown only when connected to the server).
//DONE 2. Landing page has a box to enter nickname, a button to connect
//DONE 2.1 feedback types Nickname already taken,
//DONE 2.2 also displays feedback like 'Failed to connect.', 'Server unavailable.' or 'Disconnected by the server due to inactivity.'.
//DONE 3. Chat page displays messages from the server together with the sender's nickname
//DONE 3.1 (but no messages from before the user's current session started),
//DONE 3.2 a box to enter a message, and a button to disconnect from the server
//DONE 3.3 a button to send it
//DONE 4. Does not have any inactivity timeouts. Actually not sure what this means here, need clarification
//DONE 5. Should display landing page if it's disconnected from the server.


//Things I would like to improve
//TODO not responsive
//TODO all that animation stuff too hacky, better ways need to be explored
//TODO need to break up view in to smaller parts as it is hard to follow now
//TODO add css-in-js solution for data/state driven styles?
//TODO scrolling to older messages not possible due to chosen style
//TODO if you open when server is down it will never connect even if server went up
//DONE add e2e tests with something like pupeteer


