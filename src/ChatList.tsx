import React, {PureComponent} from "react";
import {IChatMessage} from "./ChatState";
import {List, ListItem, Avatar, FontIcon} from 'react-md';

interface ChatListProps {
    messages: IChatMessage[];
}

class ChatListMessage extends PureComponent<IChatMessage> {
    render() {
        return (
            <ListItem
                leftAvatar={<Avatar>{this.props.who}</Avatar>}
                primaryText={this.props.text}
                secondaryText={new Date(this.props.when).toLocaleString()}
                threeLines
            />
        );
    }
}

export class ChatList extends PureComponent<ChatListProps> {

    render() {
        return (
            <List style={{
                position: 'absolute',
                bottom: 0,
                left: 0,
                right: 0,
            }}>
                {this.props.messages.map(message => (<ChatListMessage {...message}/>))}
            </List>
        );
    }
}