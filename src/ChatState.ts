const io: any = (window as any)['io'];


export interface IChatMessage {
    who: string;
    when: number;
    text: string;
}

export class ChatState {
    error: string = "";
    socket: any;
    chat: IChatMessage[] = [];

    constructor(private onChange: (state: ChatState) => void) {
    }

    connect(name: string) {
        this.error = "";
        try {
            this.socket = io('http://localhost:3002', {
                query: {
                    name: name,
                },
            });

            this.socket.on('chat message', (msg: string) => {
                this.chat.push(JSON.parse(msg));
                this.changed();
            });

            this.socket.on('closeReason', (msg: string) => {
                this.error = msg;
                this.disconnect();
            });

            this.socket.on('connect_error', (e: any) => {
                if (this.socket) {
                    if (!this.error) {
                        this.error = 'Connection error';
                    }
                    this.socket.io.reconnection(false);

                    this.cleanupAfterDisconnect();
                    this.changed();
                }
            });

            this.socket.on('connect', () => this.changed());
        } catch (e) {
            this.error = 'Failed to connect';
            this.changed();
        }
    }

    cleanupAfterDisconnect() {
        this.socket = undefined;
        this.chat = [];
    }

    disconnect() {
        if (this.socket) {
            this.socket.disconnect();
            this.cleanupAfterDisconnect();
            this.changed();
        }
    }

    changed() {
        this.onChange(this);
    }

    sendMessage(msg:string) {
        if (this.socket) {
            this.socket.emit('chat message', msg);
        }
    }
}