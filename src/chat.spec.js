const puppeteer = require('puppeteer');

async function loginWithName(page, name) {
    const [login, nickname] = await Promise.all([page.$('#loginLogOut'), await page.$('#nickname')]);

    await nickname.focus();
    await page.keyboard.type(name);

    await login.click();
}

describe('Chat', () => {
    let browser;
    let page;

    beforeAll(async () => {
        browser = await puppeteer.launch({
            headless: false
        });

        page = await browser.newPage();

        page.emulate({
            viewport: {
                width: 1080,
                height: 720
            },
            userAgent: ''
        });
    });


    describe('Landing', () => {
        beforeEach(async () => {
            await page.goto('http://localhost:3000/');
            await page.waitForSelector('#root h2');
        });

        test('has h2 with Chat text', async () => {
            const html = await page.$eval('#root h2', e => e.innerHTML);
            expect(html).toBe('Chat');
        }, 16000);

        test('has button with text join', async () => {
            const html = await page.$eval('#loginLogOut', e => e.innerText);
            expect(html).toBe('JOIN');
        }, 16000);

        test('joining with empty nickname results in error', async () => {
            const login = await page.$('#loginLogOut');
            login.click();

            await page.waitForSelector('.md-text-field-message');

            const html = await page.$eval('.md-text-field-message', e => e.innerHTML);
            expect(html).toBe('User name not provided');
        }, 16000);

        test('second user connecting with same name should get an error', async () => {
            const userName = 'User' + Math.random();

            await loginWithName(page, userName);
            await page.waitForFunction(
                'document.querySelector("#loginLogOut").innerText.includes("LOGOUT")'
            );

            const page2 = await browser.newPage();

            page2.emulate({
                viewport: {
                    width: 1080,
                    height: 720
                },
                userAgent: ''
            });

            await page2.goto('http://localhost:3000/');
            await page2.waitForSelector('#root h2');

            await loginWithName(page2, userName);
            await page2.waitForSelector('.md-text-field-message');
            const html = await page2.$eval('.md-text-field-message', e => e.innerHTML);
            expect(html).toBe('User name already taken');

        }, 16000);
    });

    afterAll(() => {
        browser.close();
    })
});